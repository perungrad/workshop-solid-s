<?php declare(strict_types=1);

namespace Workshop\Example2\Database;

interface DatabaseConnectionInterface
{
    /**
     * @param string $tableName
     * @param array  $values
     *
     * @return int
     */
    public function insert($tableName, array $values): int;
}

