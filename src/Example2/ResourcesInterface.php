<?php declare(strict_types=1);

namespace Workshop\Example2;

interface ResourcesInterface
{
    public function getResourceAmount(int $resourceType): int;
}

