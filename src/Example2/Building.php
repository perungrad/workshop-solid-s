<?php declare(strict_types=1);

namespace Workshop\Example2;

use Workshop\Example2\Exceptions\InvalidLevelException;
use Workshop\Example2\ResourcesInterface;
use Workshop\Example2\TownInterface;
use Workshop\Example2\Database\DatabaseConnectionInterface;

class Building
{
    /** @var int */
    private $id;

    /** @var int */
    private $type;

    /** @var string */
    private $name;

    /** @var int */
    private $level;

    /** @var TownInterface */
    private $town;

    /** @var array */
    private $priceTable;

    /** @var DatabaseConnectionInterface */
    private $databaseConnection;

    public function __construct(DatabaseConnectionInterface $databaseConnection, array $priceTable)
    {
        $this->databaseConnection = $databaseConnection;
        $this->priceTable = $priceTable;
    }

    public function setId(int $id): Building
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setType(int $type): Building
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setName(string $name): Building
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setLevel(int $level): Building
    {
        $this->level = $level;

        return $this;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setTown(TownInterface $town): Building
    {
        $this->town = $town;

        return $this;
    }

    public function getTown(): TownInterface
    {
        return $this->town;
    }

    /**
     * @param int $level
     *
     * @throws InvalidLevelException
     *
     * @return ResourcesInterface
     */
    public function getPrice(int $level): ResourcesInterface
    {
        if (array_key_exists($level, $this->priceTable)) {
            return $this->priceTable[$level];
        }

        throw new InvalidLevelException();
    }

    public function build(): Building
    {
        $id = $this->databaseConnection->insert(
            'building',
            [
                'type%i'    => $this->type,
                'level%i'   => 1,
                'town_id%i' => $this->town->getId(),
            ]
        );

        $this->id = $id;

        return $this;
    }

    public function upgrade(): Building
    {
        $this->databaseConnection
            ->update(
                'building',
                [
                    'level%sql' => 'level + 1',
                ]
            )
            ->where('id = %i', $this->id);

        return $this;
    }
}

