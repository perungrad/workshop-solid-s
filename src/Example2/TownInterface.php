<?php declare(strict_types=1);

namespace Workshop\Example2;

interface TownInterface
{
    public function setId(int $id): TownInterface;

    public function getId(): int;
}

