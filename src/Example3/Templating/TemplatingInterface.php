<?php declare(strict_types=1);

namespace Workshop\Example3\Templating;

interface TemplatingInterface
{
    public function render(string $templateName, array $params = []);
}
