<?php declare(strict_types=1);

namespace Workshop\Example3\Http;

class Response implements ResponseInterface
{
    /** @var string */
    private $responseBody;

    /**
     * @param string $responseBody
     */
    public function __construct(string $responseBody)
    {
        $this->responseBody = $responseBody;
    }

    /* other useful methods */
}
