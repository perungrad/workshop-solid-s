<?php declare(strict_types=1);

namespace Workshop\Example3\Http;

interface RequestInterface
{
    /**
     * @param string $paramName
     * @param string $defaultValue
     *
     * @return string
     */
    public function getParam(string $paramName, string $defaultValue = null): string;

    /**
     * @param string $methodName
     *
     * @return bool
     */
    public function isMethod(string $methodName): bool;
}

