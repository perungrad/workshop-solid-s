<?php declare(strict_types=1);

namespace Workshop\Example3\Http;

class RedirectResponse
{
    /** @var string */
    private $redirectUrl;

    /** @var int */
    private $statusCode;

    /**
     * @param string $redirectUrl
     * @param int    $statusCode
     */
    public function __construct(string $redirectUrl, int $statusCode = 302)
    {
        $this->redirectUrl = $redirectUrl;
        $this->statusCode  = $statusCode;
    }

    /* other useful methods */
}

