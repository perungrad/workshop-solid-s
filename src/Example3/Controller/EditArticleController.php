<?php declare(strict_types=1);

namespace Workshop\Example3\Controller;

use Workshop\Example3\Database\DatabaseConnectionInterface;
use Workshop\Example3\Entity\Article;
use Workshop\Example3\Exceptions\Status404NotFoundException;
use Workshop\Example3\Http\RedirectResponse;
use Workshop\Example3\Http\RequestInterface;
use Workshop\Example3\Http\Response;
use Workshop\Example3\Http\ResponseInterface;
use Workshop\Example3\Templating\TemplatingInterface;
use Workshop\Example3\Translator\TranslatorInterface;

class EditArticleController
{
    /** @var DatabaseConnectionInterface */
    private $databaseConnection;

    /** @var TranslatorInterface */
    private $translator;

    /** @var TemplatingInterface */
    private $templating;

    /**
     * @param DatabaseConnectionInterface $databaseConnection
     * @param TranslatorInterface         $translator
     * @param TemplatingInterface         $templating
     */
    public function __construct(DatabaseConnectionInterface $databaseConnection, TranslatorInterface $translator, TemplatingInterface $templating)
    {
        $this->databaseConnection = $databaseConnection;
        $this->translator         = $translator;
        $this->templating         = $templating;
    }

    /**
     * @param int              $articleId
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    public function editArticleAction(int $articleId, RequestInterface $request): ResponseInterface
    {
        $query = $this->databaseConnection
            ->select('id, title, text')
            ->from('article')
            ->where('id = %i', $articleId);

        $result = $query->execute();

        $row = $result->fetchRow();

        $article = null;

        if ($row) {
            $article = new Article();
            $article->setId($row['id'])
                ->setTitle($row['title'])
                ->setText($row['text']);
        }

        if (!$article) {
            throw new Status404NotFoundException();
        }

        if ($request->isMethod('POST')) {
            $title = $request->getParam('title', '');
            $text  = $request->getParam('text', '');

            if ((trim($title) != '') && (trim($text) != '')) {
                $article->setTitle($title)
                    ->setText($text);

                $this->databaseConnection
                    ->update(
                        'article',
                        [
                            'title%s' => $title,
                            'text%s'  => $text,
                        ]
                    )
                    ->where('id = %i', $articleId)
                    ->execute();

                if (!array_key_exists('flash_messages', $_SESSION)) {
                    $_SESSION['flash_messages'] = [];
                }

                if (!array_key_exists('info', $_SESSION['flash_messages'])) {
                    $_SESSION['flash_messages']['info'] = [];
                }

                $_SESSION['flash_messages']['info'][] = $this->translator->trans('Article was updated');

                $detailUrl = '/article/' . $article->getTitle();

                return new RedirectResponse($detailUrl);
            }
        }

        $html = $this->templating->render(
            'article/edit.html.twig',
            [
                'article' => $article,
            ]
        );

        return new Response($html);
    }
}

