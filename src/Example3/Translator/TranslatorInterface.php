<?php declare(strict_types=1);

namespace Workshop\Example3\Translator;

interface TranslatorInterface
{
    public function trans(string $key): string;
}
