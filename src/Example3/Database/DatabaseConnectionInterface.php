<?php declare(strict_types=1);

namespace Workshop\Example3\Database;

interface DatabaseConnectionInterface
{
    public function insert(string $tableName, array $values);

    public function update(string $tableName, array $values);

    public function select(string $columns);
}

