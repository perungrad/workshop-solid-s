<?php declare(strict_types=1);

namespace Workshop\Example1\Templating;

interface TemplatingInterface
{
    /**
     * @param string $templateName
     * @param array  $params
     *
     * @return string
     */
    public function render(string $templateName, array $params): string;
}

