<?php declare(strict_types=1);

namespace Workshop\Example1\User;

class User
{
    /** @var string */
    private $emailAddress;

    /**
     * @param string $emailAddress
     *
     * @return self
     */
    public function setEmailAddress(string $emailAddress): User
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }
}

