<?php declare(strict_types=1);

namespace Workshop\Example1\Mailer;

interface MailerInterface
{
    /**
     * @param MessageInterface $message
     */
    public function send(MessageInterface $message);
}

