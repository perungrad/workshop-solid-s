<?php declare(strict_types=1);

namespace Workshop\Example1\Mailer;

interface MessageInterface
{
    /**
     * @param string $emailAddress
     *
     * @return self
     */
    public function setTo(string $emailAddress): MessageInterface;
}

