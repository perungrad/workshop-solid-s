<?php declare(strict_types=1);

namespace Workshop\Example1\Mailer;

use Workshop\Example1\Mailer\MessageInterface;

class Message implements MessageInterface
{
    /** @var string */
    private $subject;

    /** @var string */
    private $body;

    /** @var string */
    private $emailTo;

    /**
     * @param string $subject
     * @param string $body
     */
    public function __construct(string $subject, string $body)
    {
        $this->subject = $subject;
        $this->body    = $body;
    }

    /**
     * @param string $emailAddress
     *
     * @return self
     */
    public function setTo(string $emailAddress): MessageInterface
    {
        $this->emailTo = $emailAddress;

        return $this;
    }
}

