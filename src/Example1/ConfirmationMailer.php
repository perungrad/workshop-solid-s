<?php declare(strict_types=1);

namespace Workshop\Example1;

use Workshop\Example1\Mailer\MailerInterface;
use Workshop\Example1\Mailer\Message;
use Workshop\Example1\Mailer\MessageInterface;
use Workshop\Example1\Templating\TemplatingInterface;
use Workshop\Example1\Translator\TranslatorInterface;
use Workshop\Example1\User\User;

class ConfirmationMailer
{
    /** @var TemplatingInterface */
    private $templating;

    /** @var TranslatorInterface */
    private $translator;

    /** @var MailerInterface */
    private $mailer;

    /**
     * @param TemplatingInterface $templating
     * @param TranslatorInterface $translator
     * @param MailerInterface     $mailer
     */
    public function __construct(TemplatingInterface $templating, TranslatorInterface $translator, MailerInterface $mailer)
    {
        $this->templating = $templating;
        $this->translator = $translator;
        $this->mailer     = $mailer;
    }

    /**
     * @param User $user
     */
    public function sendTo(User $user)
    {
        $subject = $this->translator->translate('Confirm your email address');

        $body = $this->templating->render(
            'confirmationEmail.tpl',
            [
                'user' => $user,
            ]
        );

        $message = new Message($subject, $body);

        $message->setTo($user->getEmailAddress());

        $this->mailer->send($message);
    }
}

