<?php declare(strict_types=1);

namespace Workshop\Example1\Translator;

interface TranslatorInterface
{
    /**
     * @param string $message
     *
     * @return string
     */
    public function translate(string $message): string;
}

